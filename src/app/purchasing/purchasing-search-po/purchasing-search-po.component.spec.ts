import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasingSearchPoComponent } from './purchasing-search-po.component';

describe('PurchasingSearchPoComponent', () => {
  let component: PurchasingSearchPoComponent;
  let fixture: ComponentFixture<PurchasingSearchPoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasingSearchPoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasingSearchPoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
