import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasingReviewComponent } from './purchasing-review.component';

describe('PurchasingReviewComponent', () => {
  let component: PurchasingReviewComponent;
  let fixture: ComponentFixture<PurchasingReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasingReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasingReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
