import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasingDashboardComponent } from './purchasing-dashboard.component';

describe('PurchasingDashboardComponent', () => {
  let component: PurchasingDashboardComponent;
  let fixture: ComponentFixture<PurchasingDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasingDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasingDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
