import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasingByOrderComponent } from './purchasing-by-order.component';

describe('PurchasingByOrderComponent', () => {
  let component: PurchasingByOrderComponent;
  let fixture: ComponentFixture<PurchasingByOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasingByOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasingByOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
