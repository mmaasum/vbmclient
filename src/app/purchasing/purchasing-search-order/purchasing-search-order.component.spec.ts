import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasingSearchOrderComponent } from './purchasing-search-order.component';

describe('PurchasingSearchOrderComponent', () => {
  let component: PurchasingSearchOrderComponent;
  let fixture: ComponentFixture<PurchasingSearchOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasingSearchOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasingSearchOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
