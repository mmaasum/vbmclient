import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasingNewOrderComponent } from './purchasing-new-order.component';

describe('PurchasingNewOrderComponent', () => {
  let component: PurchasingNewOrderComponent;
  let fixture: ComponentFixture<PurchasingNewOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasingNewOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasingNewOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
