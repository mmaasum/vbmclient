import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatchingProductionComponent } from './patching-production.component';

describe('PatchingProductionComponent', () => {
  let component: PatchingProductionComponent;
  let fixture: ComponentFixture<PatchingProductionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatchingProductionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatchingProductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
