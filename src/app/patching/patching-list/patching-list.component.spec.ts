import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatchingListComponent } from './patching-list.component';

describe('PatchingListComponent', () => {
  let component: PatchingListComponent;
  let fixture: ComponentFixture<PatchingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatchingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatchingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
