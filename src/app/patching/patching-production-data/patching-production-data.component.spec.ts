import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatchingProductionDataComponent } from './patching-production-data.component';

describe('PatchingProductionDataComponent', () => {
  let component: PatchingProductionDataComponent;
  let fixture: ComponentFixture<PatchingProductionDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatchingProductionDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatchingProductionDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
