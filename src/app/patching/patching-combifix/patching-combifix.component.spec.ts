import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatchingCombifixComponent } from './patching-combifix.component';

describe('PatchingCombifixComponent', () => {
  let component: PatchingCombifixComponent;
  let fixture: ComponentFixture<PatchingCombifixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatchingCombifixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatchingCombifixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
