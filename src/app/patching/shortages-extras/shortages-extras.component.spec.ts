import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortagesExtrasComponent } from './shortages-extras.component';

describe('ShortagesExtrasComponent', () => {
  let component: ShortagesExtrasComponent;
  let fixture: ComponentFixture<ShortagesExtrasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortagesExtrasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortagesExtrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
