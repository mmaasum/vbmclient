import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatchingDashboardComponent } from './patching-dashboard.component';

describe('PatchingDashboardComponent', () => {
  let component: PatchingDashboardComponent;
  let fixture: ComponentFixture<PatchingDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatchingDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatchingDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
