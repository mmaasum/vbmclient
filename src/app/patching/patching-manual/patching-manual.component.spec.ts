import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatchingManualComponent } from './patching-manual.component';

describe('PatchingManualComponent', () => {
  let component: PatchingManualComponent;
  let fixture: ComponentFixture<PatchingManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatchingManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatchingManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
