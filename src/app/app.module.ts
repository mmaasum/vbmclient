import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WrapperLayoutComponent } from './shared/layouts/wrapper-layout/wrapper-layout.component';
import { MenuBarComponent } from './shared/layouts/menu-bar/menu-bar.component';
import { LoginComponent } from './shared/layouts/login/login.component';
import { PurchasingDashboardComponent } from './purchasing/purchasing-dashboard/purchasing-dashboard.component';
import { PurchasingNewOrderComponent } from './purchasing/purchasing-new-order/purchasing-new-order.component';
import { PurchasingSearchOrderComponent } from './purchasing/purchasing-search-order/purchasing-search-order.component';
import { PurchasingDetailsComponent } from './purchasing/purchasing-details/purchasing-details.component';
import { PurchasingReviewComponent } from './purchasing/purchasing-review/purchasing-review.component';
import { PurchasingByOrderComponent } from './purchasing/purchasing-by-order/purchasing-by-order.component';
import { PurchasingSearchPoComponent } from './purchasing/purchasing-search-po/purchasing-search-po.component';
import { PatchingDashboardComponent } from './patching/patching-dashboard/patching-dashboard.component';
import { PatchingListComponent } from './patching/patching-list/patching-list.component';
import { PatchingProductionDataComponent } from './patching/patching-production-data/patching-production-data.component';
import { PatchingCombifixComponent } from './patching/patching-combifix/patching-combifix.component';
import { PatchingManualComponent } from './patching/patching-manual/patching-manual.component';
import { ShortagesExtrasComponent } from './patching/shortages-extras/shortages-extras.component';
import { PatchingProductionComponent } from './patching/patching-production/patching-production.component';



@NgModule({
  declarations: [
    AppComponent,
    WrapperLayoutComponent,
    MenuBarComponent,
    LoginComponent,
    PurchasingDashboardComponent,
    PurchasingNewOrderComponent,
    PurchasingSearchOrderComponent,
    PurchasingDetailsComponent,
    PurchasingReviewComponent,
    PurchasingByOrderComponent,
    PurchasingSearchPoComponent,
    PatchingDashboardComponent,
    PatchingListComponent,
    PatchingProductionDataComponent,
    PatchingCombifixComponent,
    PatchingManualComponent,
    ShortagesExtrasComponent,
    PatchingProductionComponent,
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
