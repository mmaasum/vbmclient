import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WrapperLayoutComponent } from './shared/layouts/wrapper-layout/wrapper-layout.component';
import { LoginComponent } from './shared/layouts/login/login.component'
import { PurchasingDashboardComponent } from './purchasing/purchasing-dashboard/purchasing-dashboard.component';
import { PurchasingNewOrderComponent } from './purchasing/purchasing-new-order/purchasing-new-order.component';
import { PurchasingSearchOrderComponent } from './purchasing/purchasing-search-order/purchasing-search-order.component';
import { PurchasingDetailsComponent } from './purchasing/purchasing-details/purchasing-details.component';
import { PurchasingByOrderComponent } from './purchasing/purchasing-by-order/purchasing-by-order.component';
import { PurchasingReviewComponent } from './purchasing/purchasing-review/purchasing-review.component';
import { PurchasingSearchPoComponent } from './purchasing/purchasing-search-po/purchasing-search-po.component';
import { PatchingDashboardComponent } from './patching/patching-dashboard/patching-dashboard.component';
import { PatchingListComponent } from './patching/patching-list/patching-list.component';
import { PatchingProductionDataComponent } from './patching/patching-production-data/patching-production-data.component';
import { PatchingCombifixComponent } from './patching/patching-combifix/patching-combifix.component';
import { PatchingManualComponent } from './patching/patching-manual/patching-manual.component';
import { ShortagesExtrasComponent } from './patching/shortages-extras/shortages-extras.component';
import { PatchingProductionComponent } from './patching/patching-production/patching-production.component';






// const routes: Routes = [];

const routes: Routes = [
	{path: '', component: LoginComponent},
	{
		path: 'dashboard',
		component: WrapperLayoutComponent,
		// children: [
		// 	{ path: '', component: HomeComponent, canActivate: [AuthGuard] },
		// 	{ path: 'home', component: HomeComponent, canActivate: [AuthGuard] }			
		// ]
	},
	{ path:'purchasing-dashboard', component:PurchasingDashboardComponent },
	{ path:'new-purchasing-order', component:PurchasingNewOrderComponent},
	{ path:'new-purchasing-search', component:PurchasingSearchOrderComponent},
	{ path:'purchasing-details', component:PurchasingDetailsComponent},
	{ path:'purchasing-by-order', component: PurchasingByOrderComponent},
	{path:'purchasing-review', component:PurchasingReviewComponent},
	{path:'purchasing-search-po', component:PurchasingSearchPoComponent},
	{path:'patching-dashboard', component:PatchingDashboardComponent},
	{path:'patching-list', component:PatchingListComponent},
	{path:'patching-production-data', component:PatchingProductionDataComponent},
	{path:'patching-combifix', component:PatchingCombifixComponent },
	{path:'patching-manual', component:PatchingManualComponent},
	{path:'shortages-extras', component:ShortagesExtrasComponent},
	{path:'patching-production', component:PatchingProductionComponent}
	// { path: 'login/:clientId/:secretKey', pathMatch: 'full', component: LoginComponent },
	// { path: 'login', pathMatch: 'full', component: LoginComponent },
	// { path: 'unauthorized', component: UnauthorizedComponent },
	// { path: '**', component: PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
